﻿#include <iostream>
using namespace std;

class Animal
{
private:


public:
    
    virtual void Voice() const = 0;
    virtual ~Animal() {};

};
class Frog : public Animal
{
public:
    void Voice() const override
    {
        cout << "Croak\n";
    }
    ~Frog() { cout << "Frog destructor" << endl; }
};
class Mouse : public Animal
{
public:
    void Voice() const override
    {
        cout << "Squeek\n";
    }
    ~Mouse() { cout << "Mouse destructor" << endl; }
};
class Fox : public Animal
{
public:
    void Voice() const override
    {
        cout << "Gering-ding-ding-ding-dingeringeding\n";
    }
    ~Fox() { cout << "Fox destructor" << endl; }
};



int main()
{
    Animal* Animals[3];
    Animals[0] = new Frog();
    Animals[1] = new Mouse();
    Animals[2] = new Fox();

    for (Animal* a : Animals)
        a->Voice();

    delete  Animals[0]; delete Animals[1]; delete Animals[2];
}
